import React, {Component} from 'react';

const HourlyCondition = (props) => (
    <div className="hourly-block">
        <span>{props.temp}</span>
        <div class="temperature-line"></div>
        <span>{props.hour}</span>
    </div>
);

export default HourlyCondition;