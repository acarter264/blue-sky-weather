import React from 'react';
import {Button} from 'reactstrap';
import { WeatherIcon, AlertBanner, ForecastDayItem, HourlyCondition } from './shared/index';
import {dayStateCalc, FormattedTemperature} from '../utils/index';
import {getShortHour} from '../utils/index.js';

//TODO: Find a way to check whether within sunrise or sunset hours
const CurrentForecast = ({...forecast}) => (
    <div>
        <WeatherIcon icon={forecast.icon} className="large-icon"/>
        <h1>{FormattedTemperature(forecast.currentTemp)}</h1>
        <h2>{forecast.currentForecast}</h2>
       
        {forecast.alerts && forecast.alerts.map((alert, i) => (
            i === 0 || alert[i] !== alert[i-1] ? (<AlertBanner currentWeatherAlerts={alert.title} key={i} />) : null
        ))}
        <hr/>
        <p>sunset: {forecast.sunset}</p>
        <p>current: {forecast.currentTime}</p>
        
        <p>{dayStateCalc(forecast.currentTime, forecast.sunset) ? "within shift hours" : "not within shift hours"}</p>
        <Button onClick={forecast.determineLocation}>Refresh</Button>
        <div class="hourly-block-container">
        {forecast.hourly && forecast.hourly.slice(0,23).map((hourlycondition, i) => (
            <HourlyCondition temp={FormattedTemperature(hourlycondition.temperature)} hour={getShortHour(hourlycondition.time)} key={i}/>
        ))}
        </div>
    </div>      
);

export default CurrentForecast;