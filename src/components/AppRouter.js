import React from "react";
import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
import HomeView from "./HomeView";
import SevenDayForecast from "./SevenDayForecast";
import AppSettings from "./AppSettings";
import LocationsView from "./LocationsView";

const AppRouter = ({...props}) => (
  
  <Router>
    <div>

      <Route path="/" exact render={() => <HomeView {...props}/> }/>
      <Route path="/forecast/" render={() => <SevenDayForecast {...props}/> } />
      <Route path="/locations/" render={() => <LocationsView/>} />
      <Route path="/settings/" render={() => <AppSettings/>} />

      <nav className="app-router-nav">
        <ul className="forecast-panel-menu">
          <li>
            <NavLink to="/" exact="true" activeClassName="active-route"><i className="fa fa-sun-o" aria-hidden="true"></i> <span className="route-name-text"> Home</span></NavLink>
          </li>
          <li>
            <NavLink to="/forecast/" activeClassName="active-route"><i className="fa fa-hourglass" aria-hidden="true"></i><span className="route-name-text"> Forecast</span></NavLink>
          </li>
          <li>
            <NavLink to="/locations/" activeClassName="active-route"><i className="fa fa-map-marker" aria-hidden="true"></i><span className="route-name-text"> Location</span></NavLink>
          </li>
          <li>
            <NavLink to="/settings/" activeClassName="active-route"><i className="fa fa-gear" aria-hidden="true"></i><span className="route-name-text"> Settings</span></NavLink>
          </li>
        </ul>
      </nav>
    </div>
  </Router>
);

const revealText = (bool) => {

}

export default AppRouter;